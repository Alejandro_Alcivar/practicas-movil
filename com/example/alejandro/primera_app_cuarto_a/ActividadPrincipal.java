package com.example.alejandro.primera_app_cuarto_a;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Vibrator;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;


public class ActividadPrincipal extends AppCompatActivity {

    Button botonIngresar, botonGuardar, botonBuscar, botonParametro, botonfragmento, botonAutenticar, botonVibrar, botonSensor;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_actividad_principal);

        final Vibrator vibrator = (Vibrator)this.getSystemService(Context.VIBRATOR_SERVICE);

        botonGuardar = findViewById(R.id.guardar);
        botonIngresar = findViewById(R.id.ingresar);
        botonBuscar = findViewById(R.id.buscar);
        botonParametro = findViewById(R.id.pasarpa);
        botonfragmento = findViewById(R.id.fragmento);
        botonVibrar = findViewById(R.id.vibrar);
        botonSensor = findViewById(R.id.sensor);

        botonSensor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ActividadPrincipal.this, acelerometro.class);
                startActivity(intent);
            }
        });


        botonParametro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ActividadPrincipal.this, ActividadPasarParametro.class);
                startActivity(intent);
            }
        });

        botonIngresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ActividadPrincipal.this, Ingresar.class );
                startActivity(intent);
            }
        });

        botonGuardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ActividadPrincipal.this, Guardar.class );
                startActivity(intent);
            }
        });

        botonBuscar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ActividadPrincipal.this, Buscar.class );
                startActivity(intent);
            }
        });

        botonfragmento.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ActividadPrincipal.this, Fragmentos.class);
                startActivity(intent);
            }
        });

        botonVibrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                vibrator.vibrate(600);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected (MenuItem item){
        Intent intent;
        switch (item.getItemId()){
            case R.id.opcionLogin:
                Dialog dialogoLogin= new Dialog(ActividadPrincipal.this);
                dialogoLogin.setContentView(R.layout.activity_ingresar);
                dialogoLogin.show();



                botonAutenticar = dialogoLogin.findViewById(R.id.button2);
                final EditText cajaUsuario = dialogoLogin.findViewById(R.id.editText);
                final EditText cajaClave = dialogoLogin.findViewById(R.id.editText5);
                final Vibrator vibrator = (Vibrator)this.getSystemService(Context.VIBRATOR_SERVICE);

                botonAutenticar.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Toast.makeText(ActividadPrincipal.this, cajaUsuario.getText().toString() +""+ cajaClave.getText().toString(),Toast.LENGTH_LONG).show();
                        vibrator.vibrate(600);
                    }
                });

                break;
            case R.id.opcionRegistrar:
                intent = new Intent(ActividadPrincipal.this, Guardar.class);
                startActivity(intent);
                break;
        }
        return true;
    }

}