package com.example.alejandro.primera_app_cuarto_a;

import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;


public class acelerometro extends AppCompatActivity implements SensorEventListener {


    TextView texto;
    SensorManager sensorManager;
    private Sensor acelerometro;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_acelerometro);
        texto = findViewById(R.id.IbITexto);

        sensorManager = (SensorManager)getSystemService(SENSOR_SERVICE);
        acelerometro = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);



    }

    @Override
    public void onAccuracyChanged(android.hardware.Sensor sensor, int accuracy) {

    }

    @Override
    protected void onPause(){
        super.onPause();
        sensorManager.unregisterListener(this);
    }

    @Override
    protected void onResume (){
        super.onResume();
        sensorManager.registerListener(this, acelerometro,sensorManager.SENSOR_DELAY_NORMAL);
    }

    @Override
    public void onSensorChanged(SensorEvent event){
        float x,y,z;
        x = event.values[0];
        y = event.values[1];
        z = event.values[2];
        texto.setText("");
        texto.append("\n El valor de x " + x + "\n El valor de y " + y + "\n El valor de z " + z );

    }
}
